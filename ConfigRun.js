// conf.js
var HtmlReporter = require('protractor-beautiful-reporter');
var path = require('path');
exports.config = {
framework: 'jasmine',
//seleniumAddress: 'http://localhost:4444/wd/hub',
specs: ['StagingXLogin.js'],
capabilities: {
    browserName: 'chrome'
  },
onPrepare: function () {
  browser.ignoreSynchronization = true
  browser.driver.manage().window().maximize();
    // Add a screenshot reporter and store screenshots to `/tmp/screenshots`:
    jasmine.getEnv().addReporter(new HtmlReporter({
        baseDirectory: 'tmp/screenshots'
        ,jsonsSubfolder: 'jsons'
        ,screenshotsSubfolder: 'images'
      //  , pathBuilder: function pathBuilder(spec, descriptions, results, capabilities) {
            // Return '<browser>/<specname>' as path for screenshots:
            // Example: 'firefox/list-should work'.
      //  return path.join(capabilities.caps_.browser, descriptions.join('-'));
      //  }
    }).getJasmine2Reporter());
},
};